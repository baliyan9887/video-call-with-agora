export const rtc = {
    // For the local client
    client: null,
    // For the local audio and video tracks
    joined: false,
    published: false,
    localStream: null,
    remoteStreams: [],
    params: {},
    localAudioTrack: null,
    localVideoTrack: null,
  };
   
  export const options = {
    // Pass your app ID here
    appId: "9c78b450a0b948fa9446aafaeef6f3bd",
    // Pass a token if your project enables the App Certificate
    token: "0069c78b450a0b948fa9446aafaeef6f3bdIADFynnm0Y3osu03fPm0mILpDAQh1W0NX8JIhrPD7Sa56Ax+f9gAAAAAEACpE93IefLzXwEAAQBE8vNf",
    // User ID
    uid: null,
    // Mode 
    mode: "rtc",
    codec: "h264",
    channel: "online"
  };
  
  
  // 0069c78b450a0b948fa9446aafaeef6f3bdIACVgGQSQZihgkSp+yKdLqe80zrokowNMiOry1ey5NsLA+q+Mp4AAAAAEACyc3BilJvtXwEAAQBfm+1f