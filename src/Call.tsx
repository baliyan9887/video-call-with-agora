import React, { useState } from 'react';
import AgoraRTC from 'agora-rtc-sdk-ng';
import useAgora from './hooks/useAgora';
import MediaPlayer from './components/MediaPlayer';
import MicIcon from '@material-ui/icons/Mic';
import MicOffIcon from '@material-ui/icons/MicOff';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';
import {IconButton, Typography} from '@material-ui/core';
import { options } from "./constants";
import './Call.css';

const client = AgoraRTC.createClient({ codec: 'h264', mode: 'rtc' });

function Call() {
  // const [ appid, setAppid ] = useState('');
  // const [ token, setToken ] = useState('');
  const [ channel, setChannel ] = useState('');
  const {
    localAudioTrack, 
    localVideoTrack, 
    leave, 
    join, 
    joinState, 
    remoteUsers, 
    mute,
    unMute,
    micState,
    cameraoff,
    cameraon,
    cameraState
  } = useAgora(client);

  return (
    <div className='call'>
      <form className='call-form'>
        {/* <label>
          AppID:
          <input type='text' name='appid' onChange={(event) => { setAppid(event.target.value) }}/>
        </label> */}
        {/* <label>
          Token(Optional):
          <input type='text' name='token' onChange={(event) => { setToken(event.target.value) }} />
        </label> */}
        <label>
          Channel:
          <input type='text' name='channel' onChange={(event) => { setChannel(event.target.value) }} />
        </label>
        <div className='button-group'>
          <button id='join' type='button' className='btn btn-primary btn-sm' disabled={joinState} onClick={() => {join(options.appId, channel, options.token)}}>Join</button>
          <button id='leave' type='button' className='btn btn-primary btn-sm' disabled={!joinState} onClick={() => {leave()}}>Leave</button>
        </div>
      </form>
      <div className='player-container'>
        <div className='local-player-wrapper'>
          <p className='local-player-text'>{localVideoTrack && `localTrack`}{joinState && localVideoTrack ? `(${client.uid})` : ''}</p>
          <MediaPlayer videoTrack={localVideoTrack} audioTrack={localAudioTrack}></MediaPlayer>
          
          {
            joinState
            ?
            <div>
               {
                !micState
                ?
                <>
                  <IconButton component="span" onClick={() => mute()}>
                  
                  <MicIcon /> 
                  <Typography style={{marginLeft: "10px"}}>Mic</Typography>
                </IconButton>
                </>
                :
                micState
                ?
                <IconButton component="span" onClick ={() => unMute()}>
                  <MicOffIcon />
                  <Typography style={{marginLeft: "10px"}}>Mic</Typography>
                </IconButton>
                :
                ''
              }
              {
              !cameraState
              ?
              <>
                <IconButton component="span" onClick={() => cameraoff()}>
                
                <VideocamIcon /> 
                <Typography style={{marginLeft: "10px"}}>Video</Typography>
              </IconButton>
              </>
              :
              cameraState
              ?
              <IconButton component="span" onClick ={() => cameraon()}>
                <VideocamOffIcon />
                <Typography style={{marginLeft: "10px"}}>Video</Typography>
              </IconButton>
              :
              ''
            }
            </div>
            :
            ""
          }
          
          {/* <button onClick={() => {mute()}} disabled={micState}>Mute</button>
          <button onClick={() => {unMute()}} disabled={!micState} >Unmute</button>
          <button onClick={() => {cameraoff()}} disabled={cameraState}>Camera Off</button>
          <button onClick={() => {cameraon()}} disabled={!cameraState} >Camera On</button> */}
          
        </div>
        {remoteUsers.map(user => (<div className='remote-player-wrapper' key={user.uid}>
            <p className='remote-player-text'>{`remoteVideo(${user.uid})`}</p>
            <MediaPlayer videoTrack={user.videoTrack} audioTrack={user.audioTrack}></MediaPlayer>
          </div>))}
      </div>
    </div>
  );
}

export default Call;
